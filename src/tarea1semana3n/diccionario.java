/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1semana3n;

import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author Usuario
 */
class diccionario {

    public void diccio() {
        Scanner nombre = new Scanner(System.in);
        Scanner edad = new Scanner(System.in);
        Scanner prs = new Scanner(System.in);
        HashMap dicpersonas = new HashMap();
        System.out.println("La cantidad de personas tiene un limite de 10"
                + "¡Cuantas personas desea ingresar?: ");
        int perso = prs.nextInt();
        for (int i = 0; i < perso; i++) {
            System.out.println("________________________________ ");
            System.out.println("Ingrese el nombre de la persona: ");
            String nom = nombre.nextLine();
            System.out.println("Ingrese la edad de la persona: ");
            int ed = edad.nextInt();
            dicpersonas.put(nom, ed);//llenar un diccionario         
        }
        System.out.println("Impresion de diccionario lleno: "+dicpersonas);
        Set<String> llaves = dicpersonas.keySet();//para recorrer el diccionario
        for (String llave : llaves) {//solamente me recorre los nombres 
            if ((int) dicpersonas.get(llave) % 2 == 0) {//Signo de porcentaje para sacar residuo, extaraemos edad
                System.out.println("Nombre: "+llave +"\u001B[34m"+" Numeros pares: " + dicpersonas.get(llave)+"\u001B[0m");
            } else {
                System.out.println("Nombre: "+llave +"\u001B[31m" +" Numeros impares: " + dicpersonas.get(llave)+"\u001B[0m");
            }

        }
    }
}
