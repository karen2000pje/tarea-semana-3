/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1semana3n;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Usuario
 */
class Matrizrandom {

    int fil1;
    int colum;

    public void ejermatriz() {
        Scanner ingresa = new Scanner(System.in);
        Random ran = new Random();
        System.out.println("Ingrese la cantidad de filas que desea agregar: ");
        fil1 = ingresa.nextInt();
        System.out.println("Ingrese la cantidad de columnas que desea agregar: ");
        colum = ingresa.nextInt();

        int[][] matriz1 = new int[fil1][colum];
        for (int i = 0; i < matriz1.length; i++) {
            for (int j = 0; j < matriz1[i].length; j++) {
                matriz1[i][j] = ran.nextInt(20);

            }
        }
        for (int i = 0; i < matriz1.length; i++) {
            for (int j = 0; j < matriz1[i].length; j++) {
                System.out.print(matriz1[i][j] + "\t");//para que quede en cuadros (tabla en impresion)

            }
            System.out.println();
        }
        int conta = 0;//ciclo para calcular la cantidad de nuemros 5 de la primer fila 0-0
        for (int i = 0; i < fil1; i++) {
            if (matriz1[i][0] == 5) {
                conta += 1;
            }
        }
        System.out.println("Numeros inicializados en 5: "+ conta);
    }
}
