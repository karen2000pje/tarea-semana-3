/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea1semana3n;

import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class Tarea1semana3n {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner opc = new Scanner(System.in);
        int opcion;
        boolean salir = false;
        while (!salir) {
            System.out.println("MENÚ PRINCIPAL");
            System.out.println("");
            System.out.println("1. Ejercicio matriz");
            System.out.println("2. Ejercicio diccionario");
            System.out.println("3. Salida");
            System.out.println("Ingrese la opcion que desea realizar:");
            opcion = opc.nextInt();
            switch (opcion) {

                case 1:
                    Matrizrandom ran = new Matrizrandom();//llamo la clase
                    ran.ejermatriz();
                    break;

                case 2:
                    diccionario dic = new diccionario();
                    dic.diccio();
                    break;

                case 3:
                    salir = true;
                    System.out.println("Opcion de salida");
                    break;
            }

        }
    }
}
